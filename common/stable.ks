# stable.ks
#
# Stable repos for Top Hat Linux

# Fedora
repo --name=fedora --mirrorlist=https://mirrors.fedoraproject.org/mirrorlist?repo=fedora-$releasever&arch=$basearch
repo --name=fedora-updates --mirrorlist=https://mirrors.fedoraproject.org/mirrorlist?repo=updates-released-f$releasever&arch=$basearch
#repo --name=fedora-updates-testing --mirrorlist=https://mirrors.fedoraproject.org/mirrorlist?repo=updates-testing-f$releasever&arch=$basearch
url --mirrorlist=https://mirrors.fedoraproject.org/mirrorlist?repo=fedora-$releasever&arch=$basearch

# RPM Fusion
# Free
repo --name=rpmfusion-free --mirrorlist=https://mirrors.rpmfusion.org/metalink?repo=free-fedora-$releasever&arch=$basearch
repo --name=rpmfusion-free-updates --mirrorlist=https://mirrors.rpmfusion.org/metalink?repo=free-fedora-updates-released-$releasever&arch=$basearch
#repo --name=rpmfusion-free-updates-testing --mirrorlist=https://mirrors.rpmfusion.org/metalink?repo=free-fedora-updates-testing-$releasever&arch=$basearch
repo --name=rpmfusion-free-tainted --mirrorlist=https://mirrors.rpmfusion.org/metalink?repo=free-fedora-tainted-$releasever&arch=$basearch

# Nonfree
repo --name=rpmfusion-nonfree --mirrorlist=https://mirrors.rpmfusion.org/metalink?repo=nonfree-fedora-$releasever&arch=$basearch
repo --name=rpmfusion-nonfree-updates --mirrorlist=https://mirrors.rpmfusion.org/metalink?repo=nonfree-fedora-updates-released-$releasever&arch=$basearch
#repo --name=rpmfusion-nonfree-updates-testing --mirrorlist=https://mirrors.rpmfusion.org/metalink?repo=nonfree-fedora-updates-testing-$releasever&arch=$basearch
repo --name=rpmfusion-nonfree-tainted --mirrorlist=https://mirrors.rpmfusion.org/metalink?repo=nonfree-fedora-tainted-$releasever&arch=$basearch

# RPM Sphere 
repo --name=rpmsphere-noarch --baseurl=https://github.com/rpmsphere/noarch/raw/master/
repo --name=rpmsphere-x86_64 --baseurl=https://github.com/rpmsphere/$basearch/raw/master/

# Distro COPR repo
repo --name=top-hat --baseurl=https://download.copr.fedorainfracloud.org/results/cheeseeboi/top-hat/fedora-$releasever-$basearch/

# Flatpak repos
%post --nochroot

remotes=(
	https://flathub.org/repo/flathub.flatpakrepo # Flathub
)

for r in $remotes; do 
	wget -P $INSTALL_ROOT/etc/flatpak/remotes.d/ $r
done

# Enable copr repos
sudo dnf copr enable -y --installroot=$INSTALL_ROOT cheeseeboi/top-hat >> /dev/null

%end


