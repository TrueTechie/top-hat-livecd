%include extra.ks

%packages

# System
baobab

# Hardware
simple-scan

# Theming
mint-y-*
arc-theme*
papirus-icon-theme

# Internet
polari
transmission-gtk
liferea

# Multimedia
rhythmbox
celluloid

%end
